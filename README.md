Altenwald.mk
============

Este repositorio está pensado para completar un sistema de publicación de Altenwald Books.

Para que funcione debe crearse un subdirectorio `lib` en el proyecto con el contenido de este repositorio. El `Makefile` en el directorio raíz debe ser:

```
include lib/altenwald.mk
```

Esto nos proporcionará comandos como:

```
make pdf
```

El sistema indicará los comandos necesarios a tener instalados en el sistema.

NOTA: este sistema está pensado para funcionar en Mac o GNU/Linux. 
