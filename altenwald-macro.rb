require_relative 'altenwald-macro/acronym'
require_relative 'altenwald-macro/command'
require_relative 'altenwald-macro/filename'
require_relative 'altenwald-macro/dirname'
require_relative 'altenwald-macro/function'
require_relative 'altenwald-macro/diagram'

Asciidoctor::Extensions.register do
  inline_macro AcronymMacro
  inline_macro CommandMacro
  inline_macro FilenameMacro
  inline_macro DirnameMacro
  inline_macro FunctionMacro

  block Asciidoctor::Diagram::Mermaid2BlockProcessor, :mermaid2
  block_macro Asciidoctor::Diagram::Mermaid2BlockMacroProcessor, :mermaid2
  inline_macro Asciidoctor::Diagram::Mermaid2InlineMacroProcessor, :mermaid2
end
