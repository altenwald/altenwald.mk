require 'asciidoctor/extensions' unless RUBY_ENGINE == 'opal'

include ::Asciidoctor

class AcronymMacro < Extensions::InlineMacroProcessor
  use_dsl

  named :acronym

  def process parent, target, attrs
    %(<acronym>#{attrs[1]}</acronym>)
  end
end
