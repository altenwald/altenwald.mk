require 'asciidoctor/extensions' unless RUBY_ENGINE == 'opal'

include ::Asciidoctor

class CommandMacro < Extensions::InlineMacroProcessor
  use_dsl

  named :command

  def process parent, target, attrs
    %(<command>#{attrs[1]}</command>)
  end
end
