require 'asciidoctor/extensions' unless RUBY_ENGINE == 'opal'

include ::Asciidoctor

class DirnameMacro < Extensions::InlineMacroProcessor
  use_dsl

  named :dirname

  def process parent, target, attrs
    attrs = attrs.map { |k,v| " #{k}='#{v}'" }.join("")
    %(<filename class='directory'#{attrs}>#{target}</filename>)
  end
end
