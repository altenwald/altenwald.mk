require 'asciidoctor/extensions' unless RUBY_ENGINE == 'opal'

include ::Asciidoctor

class FilenameMacro < Extensions::InlineMacroProcessor
  use_dsl

  named :filename

  def process parent, target, attrs
    attrs = attrs.map { |k,v| " #{k}='#{v}'" }.join("")
    %(<filename#{attrs}>#{target}</filename>)
  end
end
