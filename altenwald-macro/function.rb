require 'asciidoctor/extensions' unless RUBY_ENGINE == 'opal'

include ::Asciidoctor

class FunctionMacro < Extensions::InlineMacroProcessor
  use_dsl

  named :function

  def process parent, target, attrs
    %(<function>#{attrs[1]}</function>)
  end
end
