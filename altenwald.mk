OPTIONS=pdf html chunk publish clean

OBJS_ADOC = $(wildcard *.adoc)
OBJS_XML = $(OBJS_ADOC:%.adoc=_build/%.xml)
OBJS = ${OBJS_XML} _build/main.xml _build/signature.xml

TITLEPAGE_PUBLISH ?= lib/config/titlepage_pdf_publish.xml
TITLEPAGE_PDF ?= lib/config/titlepage_pdf.xml
BOOK_CSS ?= lib/config/book.css

EPUB_CONFIG = \
	$(BOOK_CSS) \
	lib/config/custom-epub3-chunk.xsl \
	lib/config/custom-epub3-elements.xsl

ASCIIDOCTOR_PARAMS ?= 

ifndef HOST_OS
  ifeq ($(OS),Windows_NT)
    HOST_OS := Windows
  else
    HOST_OS := $(shell uname)
  endif
endif

ifeq ($(HOST_OS),Windows)
  # These setting are for Windows
  SEPARATOR=;
else
  # This settings are for UNIX
  SEPARATOR=:
endif

EMPTY := 
SPACE := ${EMPTY} ${EMPTY}
LIBS := ${wildcard lib/*.jar lib/xsl/tools/lib/*.jar lib/xsl/extensions/*.jar}
CLASSPATH=${subst ${SPACE},${SEPARATOR},${LIBS}}

TTF_FONTS := ${wildcard fonts/*/*.ttf}
XML_FONTS := ${patsubst %.ttf,%.xml,${TTF_FONTS}}
OTF_FONTS := ${patsubst %.ttf,%.otf,${TTF_FONTS}}
OEBPS_FONTS := ${patsubst %,OEBPS/%,${OTF_FONTS}}

ifeq (, $(shell which xsltproc))
$(error "No xsltproc, consider doing apt install xsltproc or brew install xsltproc")
endif

ifeq (, $(shell which fop))
$(error "No fop, consider doing apt install fop or brew install fop")
endif

ifeq (, $(shell which xmlto))
$(error "No xmlto, consider doing apt install xmlto or brew install xmlto")
endif

ifeq (, $(shell which fontforge))
$(error "No fontforge, consider doing apt install fontforge or brew install fontforge")
endif

ifeq (, $(shell which asciidoctor))
$(error "No asciidoctor, consider doing apt install asciidoctor or brew install asciidoctor")
endif

ifeq (, $(shell which epubcheck))
$(error "No epubcheck, consider doing apt install epubcheck or brew install epubcheck")
endif

ifeq (, $(shell which pdffonts))
$(error "No pdffonts, consider doing apt install pdffonts or brew install pdffonts")
endif

ifeq (, $(shell which yj))
$(error "No yj, consider doing apt install yj or brew install yj")
endif

ifeq (, $(shell which jq))
$(error "No jq, consider doing apt install jq or brew install jq")
endif

ifeq (, $(shell which pandoc))
$(error "No pandoc, consider doing apt install pandoc or brew install pandoc")
endif

ifeq (, $(shell which barcode))
$(error "No barcode, consider doing apt install barcode or brew install gnu-barcode")
endif

SED ?= $(shell which gsed || which sed)

all:
	@echo "Opciones: $(OPTIONS)"

${XML_FONTS}: %.xml: %.ttf
	lib/ttf2xml $^ $@

${OTF_FONTS}: %.otf: %.ttf
	fontforge -lang=ff -c 'Open($$1); Generate($$2)' $< $@

_build:
	mkdir _build

pdf: target/main.pdf config/params_pdf.xsl _build/titlepage_pdf.xsl

publish: target/publish.pdf config/params_pdf_publish.xsl _build/titlepage_pdf_publish.xsl

_build/config.json: _build config.toml
	cat config.toml | yj -tj > _build/config.json
	## touch -d `stat -f %Sc -t %FT%T config.toml` _build/config.json

_build/signature.xml: _build/config.json
	lib/gen-signature _build/config.json _build/signature.xml

_build/main.xml: _build/config.json bibliography
	lib/gen-main _build/config.json _build/main.xml

check-bibliography: references.bib
	lib/biblatex_check.py -b references.bib -X

bibliography:
ifneq ("$(wildcard references.bib)","")
	make check-bibliography
	lib/gen-bibliography references.bib _build/references.json _build/references.xml _build/bibliography.xml
endif

_build/titlepage_pdf_publish.xsl: $(TITLEPAGE_PUBLISH) _build
	xsltproc \
		--output _build/titlepage_pdf_publish.xsl \
		lib/xsl/template/titlepage.xsl \
		$(TITLEPAGE_PUBLISH)

_build/titlepage_pdf.xsl: $(TITLEPAGE_PDF) _build
	xsltproc \
		--output _build/titlepage_pdf.xsl \
		lib/xsl/template/titlepage.xsl \
		$(TITLEPAGE_PDF)

_build/publish.pdf: $(OBJS) _build/publish.fo config/params_pdf_publish.xsl _build/titlepage_pdf_publish.xsl config/fopconfig.xml ${XML_FONTS}
	@[ -d target ] || mkdir target
	cp $(FIGURES) _build/
	CLASSPATH="${CLASSPATH}" fop -d \
		-c config/fopconfig.xml \
		-fo _build/publish.fo \
		-pdf _build/publish.pdf

target/publish.pdf: _build/publish.pdf _build/config.json
	lib/pdfbox _build/config.json _build/publish.pdf target/publish.pdf
	pdfinfo -box target/publish.pdf
	pdffonts _build/publish.pdf

_build/publish.fo: $(OBJS) config/params_pdf_publish.xsl _build/titlepage_pdf_publish.xsl _build
	(XML_CATALOG_FILES=`pwd`/lib/xsl/catalog.xml \
	 xmlto --noautosize -m config/params_pdf_publish.xsl fo _build/main.xml)
	mv main.fo _build/publish.fo

target/main.pdf: $(OBJS) $(FIGURES) _build/main.fo config/params_pdf.xsl _build/titlepage_pdf.xsl config/fopconfig.xml ${XML_FONTS}
	@[ -d target ] || mkdir target
	cp $(FIGURES) _build/
	CLASSPATH="${CLASSPATH}" fop -d \
		-c config/fopconfig.xml \
		-fo _build/main.fo \
		-pdf target/main.pdf
	pdffonts target/main.pdf

_build/main.fo: $(OBJS) config/params_pdf.xsl _build/titlepage_pdf.xsl _build
	(XML_CATALOG_FILES=`pwd`/lib/xsl/catalog.xml \
	 xmlto --noautosize -m config/params_pdf.xsl fo _build/main.xml)
	mv main.fo _build/main.fo

$(OBJS_XML): _build/%.xml: %.adoc _build/config.json
	asciidoctor -b docbook5 -d book -s -r ./lib/altenwald-macro.rb `lib/asciidoc-extensions _build/config.json` ${ASCIIDOCTOR_PARAMS}  -o $@ $<
	${SED} -i 's/xml:id/id/g' $@
	${SED} -i 's/<link xl:href/<ulink url/g' $@
	${SED} -i 's|</link>|</ulink>|g' $@
	${SED} -i 's|CO\([0-9]*\)-\([0-9]*\)|CO-'$*'-\1-\2|g' $@
	${SED} -i 's|:\\:|::|g' $@

epub: target/main.epub
	epubcheck target/main.epub

target/main.epub: $(OBJS) $(FIGURES) $(IMAGES) $(CALLOUTS) $(EPUB_CONFIG) $(OTF_FONTS) $(BOOK_CSS)
	-rm -f target/main.epub
	lib/copy-fonts $(BOOK_CSS) OEBPS $(OTF_FONTS)
	xsltproc \
		--stringparam admon.graphics 1 \
		--xinclude lib/config/custom-epub3-chunk.xsl \
		_build/main.xml
	mkdir -p OEBPS/images/callouts
	cp $(FIGURES) OEBPS
	cp $(wildcard _build/*.jpg _build/*.png) OEBPS
	cp $(IMAGES) OEBPS/images
	cp $(CALLOUTS) OEBPS/images/callouts
	cp figures/cover.png iTunesArtwork
	cp lib/config/com.apple.ibooks.display-options.xml META-INF/
	cp $(BOOK_CSS) OEBPS/docbook-epub.css
	lib/epub_math OEBPS
	zip -0X target/main.epub mimetype
	zip -9r target/main.epub iTunesArtwork OEBPS META-INF
	rm -rf OEBPS META-INF iTunesArtwork mimetype

html: target/html/index.html $(OBJS) config/params_html.xsl

target/html/index.html: $(OBJS) config/params_html.xsl $(BOOK_CSS)
	[ -d target/html ] && rm -rf target/html || true
	mkdir -p target/html
	(cd target/html; \
	 XML_CATALOG_FILES=`pwd`/lib/xsl/catalog.xml \
	 xmlto -m ../../config/params_html.xsl -o . html ../../_build/main.xml)
	cp -r figures target/html
	cp -r lib/images target/html
	cp $(BOOK_CSS) target/html

clean:
	-rm -rf target
	-rm -rf _build

.PHONY: $(OPTIONS)
