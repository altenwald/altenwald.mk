<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:d="http://docbook.org/ns/docbook"
  exclude-result-prefixes="#default d"
  version="1.0">

<xsl:import href="../xsl/xhtml5/docbook.xsl"/>
<xsl:include href="../xsl/epub3/epub3-element-mods.xsl"/>

<!-- Add here any templates that change element format.  Any
such customized templates with a @match attribute must also
have a priority="1" attribute to override the original.  -->

<!-- Add here any templates that change element formatting.
That is, any templates not copied from chunk-common.xsl or
chunk-core.xsl.  Any customized templates with a @match
attribute must also have a priority="1" attribute to
override the original. -->

</xsl:stylesheet>
