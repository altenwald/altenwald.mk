#!/bin/bash

JSON="$1"
XML="$2"

if [ -z "$JSON" ] || [ -z "$XML" ]; then
    echo "syntax: $(basename $0) <config.json> <signature.xml>"
    exit 1
fi

set -e

if [ "$DEBUG" != "" ]; then
    set -x
fi

function query {
    QUERY="$1"
    DEFAULT="$2"
    RESULT=$(jq -Mr "$QUERY" "$JSON")
    if [ "$RESULT" == "null" ]; then
        if [ -z "$DEFAULT" ]; then
            echo "Cannot find $QUERY"
            exit 1
        fi
        echo $DEFAULT
    else
        echo $RESULT
    fi
}

CHAPTERS_LEN=$(query '.book.chapters | length - 1')
LANG=$(query '.signature.lang' 'es')

case "$LANG" in
    "es") APX_TXT="Apéndices" ;;
    *) APX_TXT="Appendices" ;;
esac

CHAPTERS=''
for i in $(seq 0 $CHAPTERS_LEN); do
    NAME=$(query ".book.chapters[$i]")
    CHAPTERS="$CHAPTERS<xi:include xmlns:xi='http://www.w3.org/2001/XInclude' href='$NAME.xml' />"
done

APX_LEN=$(query '.book.appendices | length - 1')

APX=''
if [ "$APX_LEN" != "-1" ]; then
    for i in $(seq 0 $APX_LEN); do
        NAME=$(query ".book.appendices[$i]")
        APX="$APX<xi:include xmlns:xi='http://www.w3.org/2001/XInclude' href='$NAME.xml' />"
    done

    if [ ! -z "$APX" ]; then
        APX="<part><title>$APX_TXT</title>$APX</part>"
    fi
fi

cat > $XML <<EOM
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE book SYSTEM "../lib/dtd/docbookx.dtd"
[
<!ENTITY % MATHML.prefixed "INCLUDE">
<!ENTITY % MATHML.prefix "mml">
<!ENTITY % equation.content "(alt?, (graphic+|mediaobject+|mml:math))">
<!ENTITY % inlineequation.content
                "(alt?, (inlinegraphic+|inlinemediaobject+|mml:math))">
<!ENTITY % mathml PUBLIC "-//W3C//DTD MathML 3.0//EN"
        "../lib/dtd/mathml3/mathml3.dtd">
%mathml;
]>
<book lang="$(query '.signature.lang' 'es')">
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="signature.xml" />
    $CHAPTERS
    $APX
</book>
EOM
